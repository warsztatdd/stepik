# Nagłówek 1 [^1]

| kolumna 1 | kolumna 2 | kolumna 3 |
| :-------: | :-------: | :-------: |
|to         |jest       |tabela     |

```py
print("To jest kod w pythonie")
```
# Nagłówek 2

- [ ] to jest
- [ ] lista
- [x] zadań

 :cat: :pouting_cat: :smile_cat:

[to jest link do nagłówka 1](#nagłówek-1)

[to jest link do nagłówka 2](#nagłówek-2)

[^1]: a to stopka do nagłówka 1 :

